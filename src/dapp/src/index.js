import React from 'react'
import ReactDOM from 'react-dom'
import './styles.css'
import Dapp from './Dapp'

ReactDOM.render(<Dapp />, document.getElementById('root'))
