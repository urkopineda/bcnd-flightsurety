import React, { Component } from 'react'

import FlightSuretyApp from './contracts/FlightSuretyApp.json'
import config from './config.json'
import Web3 from 'web3'

const GAS = 6721975

class Dapp extends Component {
  state = {
    web3: undefined,
    flightSuretyApp: undefined,
    accounts: {},
    url: undefined,
    appAddress: undefined,
    dataAddress: undefined,
    logs: '',
    initialized: false,
    flight: 'ES1010',
    flights: [
      'ES1010',
      'ES2020',
      'ES3030',
      'ES4040',
      'ES5050',
    ],
    values: {
      operational: true,
      airline: undefined,
      passsenger: undefined,
      funds: 0,
      price: 0,
      flights: 'ES1010'
    }
  }

  componentDidMount() {
    const { url, appAddress, dataAddress } = config['localhost']
    const web3 = new Web3(new Web3.providers.WebsocketProvider(url.replace('http', 'ws')))
    const flightSuretyApp = new web3.eth.Contract(FlightSuretyApp.abi, appAddress)
    this.events(flightSuretyApp)
    this.setState({
      web3,
      flightSuretyApp,
      url,
      appAddress,
      dataAddress
    })
    web3.eth.getAccounts()
      .then((accounts) => {
        this.setState({
          accounts: {
            all: accounts,
            owner: accounts[0],
            airline: accounts[1],
            airlines: accounts.slice(2, 10),
            passsengers: accounts.slice(10, 20),
          },
          values: {
            operational: true,
            airline: accounts[2],
            passsenger: accounts[11],
            funds: 0,
            price: 0,
            flight: 'ES1010'
          }
        })
        this.initialize()
      })
  }

  initialize = () => {
    const { web3, flightSuretyApp, accounts, flights } = this.state
    const { owner, airline } = accounts
    flightSuretyApp.methods
      .registerAirline(airline)
      .send({
        from: owner
      }, (error, result) => {
        if (error) {
          this.handleLog(`ERROR: there was an error registering airline "${airline}", check console.`)
          console.error(error)
        } else {
          this.handleLog(`INFO: "registerAirline('${airline}')" returned "${result}"`)
          flightSuretyApp.methods
            .addFunds()
            .send({
              from: airline,
              gas: GAS,
              value: web3.utils.toWei('10', 'ether')
            }, (error, result) => {
              if (error) {
                this.handleLog(`ERROR: there was an error adding funds for "${airline}", check console.`)
                console.error(error)
              } else {
                this.handleLog(`INFO: "addFunds()" for airline "${airline}" returned "${result}"`)
                const promises = []
                for (let flight of flights) {
                  const registerFlight = (address, flight) => new Promise((resolve, reject) => {
                    flightSuretyApp.methods
                      .registerFlight(flight)
                      .send({
                        from: address,
                        gas: GAS
                      }, (error, result) => {
                        if (error) {
                          this.handleLog(`ERROR: there was an error registering the flight "${flight}", check console.`)
                          console.error(error)
                          reject(error)
                        } else {
                          this.handleLog(`INFO: "registerFlight('${flight}')" returned "${result}"`)
                          resolve(result)
                        }
                      })
                  })
                  promises.push(registerFlight(airline, flight))
                }
                Promise.all(promises)
                  .then(() => {
                    this.setState({
                      initialized: true
                    })
                    this.handleLog('INFO: Dapp initialized!')
                  })
              }
            })
        }
      })
  }

  events = (flightSuretyApp) => {
    flightSuretyApp.events
      .AirlineRegistered({}, (error, event) => {
        if (error) {
          console.log(error)
        } else {
          this.handleLog(`EVENT: "AirlineRegistered" fired with data "${JSON.stringify(event.returnValues)}"`)
        }
      })
    flightSuretyApp.events
      .FlightRegistered({}, (error, event) => {
        if (error) {
          console.log(error)
        } else {
          this.handleLog(`EVENT: "FlightRegistered" fired with data "${JSON.stringify(event.returnValues)}"`)
        }
      })
    flightSuretyApp.events
      .FlightBooked({}, (error, event) => {
        if (error) {
          console.log(error)
        } else {
          this.handleLog(`EVENT: "FlightBooked" fired with data "${JSON.stringify(event.returnValues)}"`)
        }
      })
    flightSuretyApp.events
      .BookingClaimed({}, (error, event) => {
        if (error) {
          console.log(error)
        } else {
          this.handleLog(`EVENT: "BookingClaimed" fired with data "${JSON.stringify(event.returnValues)}"`)
        }
      })
    flightSuretyApp.events
      .FundsWithdrawed({}, (error, event) => {
        if (error) {
          console.log(error)
        } else {
          this.handleLog(`EVENT: "FundsWithdrawed" fired with data "${JSON.stringify(event.returnValues)}"`)
        }
      })
    flightSuretyApp.events
      .FlightStatusInfo({}, (error, event) => {
        if (error) {
          console.log(error)
        } else {
          this.handleLog(`EVENT: "FlightStatusInfo" fired with data "${JSON.stringify(event.returnValues)}"`)
        }
      })
    flightSuretyApp.events
      .OracleReport({}, (error, event) => {
        if (error) {
          console.log(error)
        } else {
          this.handleLog(`EVENT: "OracleReport" fired with data "${JSON.stringify(event.returnValues)}"`)
        }
      })
    flightSuretyApp.events
      .OracleRequest({}, (error, event) => {
        if (error) {
          console.log(error)
        } else {
          this.handleLog(`EVENT: "OracleRequest" fired with data "${JSON.stringify(event.returnValues)}"`)
        }
      })
  }

  handleLog = (value) => {
    const { logs } = this.state
    this.setState({
      logs: `${logs}\n${value}\n`
    })
  }

  handleOnChangeAccounts = (id) => (event) => {
    const { accounts } = this.state
    this.setState({
      accounts: {
        ...accounts,
        [(id)]: event.target.value
      }
    })
  }

  handleOnChange = (id) => (event) => {
    this.setState({
      [(id)]: event.target.value
    })
  }

  handleIsOperational = () => {
    const { flightSuretyApp, accounts } = this.state
    flightSuretyApp.methods
      .isOperational()
      .call({
        from: accounts.owner
      }, (error, result) => {
        if (error) {
          this.handleLog('ERROR: there was an error checking if it\'s operational, check console.')
          console.error(error)
        } else {
          this.handleLog(`INFO: "isOperational()" returned "${result}"`)
        }
      })
  }

  handleSetOperational = () => {
    const { flightSuretyApp, accounts, values } = this.state
    flightSuretyApp.methods
      .setOperatingStatus(values.operational)
      .send({
        from: accounts.owner,
        gas: GAS
      }, (error, result) => {
        if (error) {
          this.handleLog('ERROR: there was an error checking if it\'s operational, check console.')
          console.error(error)
        } else {
          this.handleLog(`INFO: "setOperatingStatus(${values.operational})" returned "${result}"`)
        }
      })
  }

  handleRegisterAirline = () => {
    const { flightSuretyApp, accounts, values } = this.state
    flightSuretyApp.methods
      .registerAirline(values.airline)
      .send({
        from: accounts.airline,
        gas: GAS
      }, (error, result) => {
        if (error) {
          this.handleLog(`ERROR: there was an error adding airline "${values.airline}", check console.`)
          console.error(error)
        } else {
          this.handleLog(`INFO: "registerAirline(${values.airline})" returned "${result}"`)
        }
      })
  }

  handleAddFunds = () => {
    const { web3, flightSuretyApp, accounts, values } = this.state
    const { airline } = accounts
    flightSuretyApp.methods
      .addFunds()
      .send({
        from: airline,
        gas: GAS,
        value: web3.utils.toWei(values.funds.toString(), 'ether')
      }, (error, result) => {
        if (error) {
          this.handleLog(`ERROR: there was an error adding ${values.funds} ether for airline "${airline}", check console.`)
          console.error(error)
        } else {
          this.handleLog(`INFO: "addFunds()" ${values.funds} ether for airline "${airline}" returned "${result}"`)
        }
      })
  }

  handleRegisterFlight = () => {
    const { flightSuretyApp, accounts, flight } = this.state
    const { airline } = accounts
    flightSuretyApp.methods
      .registerFlight(flight)
      .send({
        from: airline,
        gas: GAS
      }, (error, result) => {
        if (error) {
          this.handleLog(`ERROR: there was an error registering the flight "${flight}" from airline "${airline}", check console.`)
          console.error(error)
        } else {
          this.handleLog(`INFO: "registerFlight('${flight}')" from airline "${airline}" returned "${result}"`)
        }
      })
  }

  handleBookFlight = () => {
    const { web3, flightSuretyApp, values } = this.state
    const { passsenger, price, flight } = values
    flightSuretyApp.methods
      .bookFlight(flight)
      .send({
        from: passsenger,
        gas: GAS,
        value: web3.utils.toWei(price.toString(), 'ether')
      }, (error, result) => {
        if (error) {
          this.handleLog(`ERROR: there was an error booking the flight insurance for flight "${flight}" from passenger "${passsenger}", check console.`)
          console.error(error)
        } else {
          this.handleLog(`INFO: "bookFlight('${flight}')" for passenger "${passsenger}" returned "${result}"`)
        }
      })
  }

  handleFetchFlightStatus = () => {
    const { flightSuretyApp, values } = this.state
    const { passsenger, flight } = values
    const timestamp = new Date().getTime()
    flightSuretyApp.methods
      .fetchFlightStatus(passsenger, flight, timestamp)
      .send({
        from: passsenger,
        gas: GAS
      }, (error, result) => {
        if (error) {
          this.handleLog(`ERROR: there was an error fetching the flight status for flight "${flight}" from passenger "${passsenger}", check console.`)
          console.error(error)
        } else {
          this.handleLog(`INFO: "fetchFlightStatus('${passsenger}', ${flight}', ${timestamp})" returned "${result}"`)
        }
      })
  }

  handleClaim = () => {
    const { flightSuretyApp, values } = this.state
    const { passsenger, flight } = values
    flightSuretyApp.methods
      .claim(flight)
      .send({
        from: passsenger,
        gas: GAS
      }, (error, result) => {
        if (error) {
          this.handleLog(`ERROR: there was an error claiming the flight insurance for flight "${flight}" from passenger "${passsenger}", check console.`)
          console.error(error)
        } else {
          this.handleLog(`INFO: "claim(${flight}')" for passenger "${passsenger}" returned "${result}"`)
        }
      })
  }

  handleWithdraw = () => {
    const { flightSuretyApp, values } = this.state
    const { passsenger } = values
    flightSuretyApp.methods
      .withdraw()
      .send({
        from: passsenger,
        gas: GAS
      }, (error, result) => {
        if (error) {
          this.handleLog(`ERROR: there was an error withdrawing the flight insurance for passenger "${passsenger}", check console.`)
          console.error(error)
        } else {
          this.handleLog(`INFO: "withdraw()" for passenger "${passsenger}" returned "${result}"`)
        }
      })
  }

  handleChangeValues = (id) => (event) => {
    const { values } = this.state
    this.setState({
      values: {
        ...values,
        [(id)]: event.target.value
      }
    })
  }

  handleChangeOperational = (event) => {
    const { values } = this.state
    this.setState({
      values: {
        ...values,
        operational: (event.target.value === 'true') ? true : false
      }
    })
  }

  render() {
    const { initialized, logs, accounts, flight, flights, values } = this.state

    const render = (
      <React.Fragment>
        <div className="navbar">
          <h1 className="navbar-title">FlightSurety by Urko Pineda Olmo</h1>
        </div>
        <main>
          <div className="container">
            <div className="section section-margin">
              <h2>Base</h2>
              <div className="subsection">
                <div className="element-super">
                  <p>Owner address</p>
                  <input
                    type="text"
                    value={accounts.owner}
                    onChange={this.handleOnChangeAccounts('owner')}
                  />
                  <p>Operational</p>
                  <select
                    value={values.operational === true ? 'true' : 'false'}
                    onChange={this.handleChangeOperational}
                  >
                    <option>true</option>
                    <option>false</option>
                  </select>
                </div>
                <div className="element element-vertical element-border">
                  <p className="center">Actions</p>
                  <button className="button" onClick={this.handleIsOperational}>
                    Check if it's operational
                  </button>
                  <button className="button" onClick={this.handleSetOperational}>
                    Set operational status
                  </button>
                </div>
              </div>
            </div>
            <div className="section section-margin">
              <h2 className="section-title">Airlines and Flights</h2>
              <div className="subsection">
                <div className="element-super">
                  <p>Airline address from</p>
                  <input
                    type="text"
                    value={accounts.airline}
                    onChange={this.handleOnChangeAccounts('airline')}
                  />
                  <p>Airline address to</p>
                  <select
                    value={values.airline}
                    onChange={this.handleChangeValues('airline')}
                  >
                    {accounts.airlines && accounts.airlines.map((a) =>
                      <option key={a}>{a}</option>
                    )}
                  </select>
                  <p>Flight code</p>
                  <input
                    type="text"
                    value={flight}
                    onChange={this.handleOnChange('flight')}
                  />
                  <p>Funds</p>
                  <input
                    type="number"
                    value={values.funds}
                    onChange={this.handleChangeValues('funds')}
                  />
                </div>
                <div className="element element-vertical element-border">
                  <p className="center">Actions</p>
                  <button className="button" onClick={this.handleRegisterAirline}>
                    Register an airline
                  </button>
                  <button className="button" onClick={this.handleAddFunds}>
                    Add funds
                  </button>
                  <button className="button" onClick={this.handleRegisterFlight}>
                    Register a flight
                  </button>
                </div>
              </div>
            </div>
            <div className="section section-margin">
              <h2 className="section-title">Passsengers</h2>
              <div className="subsection">
                <div className="element-super">
                  <p>Passsenger address</p>
                  <select
                    value={values.passsenger}
                    onChange={this.handleChangeValues('passsenger')}
                  >
                    {accounts.passsengers && accounts.passsengers.map((a) =>
                      <option key={a}>{a}</option>
                    )}
                  </select>
                  <p>Flight code</p>
                  <select
                    value={values.airline}
                    onChange={this.handleChangeValues('flight')}
                  >
                    {flights.map((a) =>
                      <option key={a}>{a}</option>
                    )}
                  </select>
                  <p>Price</p>
                  <input
                    type="number"
                    value={values.price}
                    onChange={this.handleChangeValues('price')}
                  />
                </div>
                <div className="element element-vertical element-border">
                  <p className="center">Actions</p>
                  <button className="button" onClick={this.handleFetchFlightStatus}>
                    Fetch flight status
                  </button>
                  <button className="button" onClick={this.handleBookFlight}>
                    Book a flight insurance
                  </button>
                  <button className="button" onClick={this.handleClaim}>
                    Claim insurance
                  </button>
                  <button className="button" onClick={this.handleRegisterFlight}>
                    Withdraw funds
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="events">
            <h2 className="section-title">Events and log</h2>
            <textarea className="events-area" value={logs} readOnly />
          </div>
        </main>
      </React.Fragment>
    )

    return (
      initialized
        ? render
        : <p>Loading...</p>
    )
  }
}

export default Dapp
