// IMPORTS
const FlightSuretyApp = require('../build/contracts/FlightSuretyApp.json')
const config = require('./config.json')
const Web3 = require('web3')
const express = require('express')
// CONSTANTS
//   # Port of the server
const PORT = 3000
//   # Gas of transactions
const GAS = 6721975
//   # Configuration values
const { url, appAddress, dataAddress } = config['localhost']
//   # Configure web3 with the provider
const web3 = new Web3(new Web3.providers.WebsocketProvider(url.replace('http', 'ws')))
//   # Set variables
const ORACLE_NUMBER = 25
const oracles = []
const statusCodes = ['10', '20', '30', '40', '50']
let added = false
//   # Deployed app and data
const flightSuretyApp = new web3.eth.Contract(FlightSuretyApp.abi, appAddress)

// EVENTS
//   # Listen to the oracle request event
flightSuretyApp.events.OracleRequest({
  fromBlock: 0
}, (error, event) => {
    if (error) {
      console.log(error)
    } else {
      processOracleRequest(event)
    }
})
flightSuretyApp.events.FlightStatusInfo({
  fromBlock: 0
}, (error) => {
    if (error) {
      console.log(error)
    } else {
      console.log('EVENT: FlightStatusInfo fired!')
    }
})

// FUNCTIONS
const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms))
//   # Get oracles index by address
const getOraclesIndexes = (address) => new Promise((resolve, reject) => {
  flightSuretyApp.methods
    .getOracleIndexes()
    .call({
      from: address
    }, (error, result) => {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    })
})
//   # Simulate flight status info getter
const fetchFlightStatusRegister = (address) => new Promise((resolve, reject) => {
  flightSuretyApp.methods
    .registerAirline(address)
    .send({
      from: owner
    }, (error, result) => {
      if (error) {
        console.error('ERROR: registering airline, check logs.')
        reject(error)
      } else {
        resolve(result)
      }
    })
})
const fetchFlightStatusAddFunds = (address) => new Promise((resolve, reject) => {
  flightSuretyApp.methods
    .addFunds()
    .send({
      from: address,
      gas: GAS,
      value: web3.utils.toWei('10', 'ether')
    }, (error, result) => {
      if (error) {
        console.error('ERROR: adding funds from airline, check logs.')
        reject(error)
      } else {
        resolve(result)
      }
    })
})
const fetchFlightStatusAddFlight = (address, flight) => new Promise((resolve, reject) => {
  flightSuretyApp.methods
    .registerFlight(flight)
    .send({
      from: address,
      gas: GAS
    }, (error, result) => {
      if (error) {
        console.error('ERROR: adding flight, check logs.')
        reject(error)
      } else {
        resolve(result)
      }
    })
})
const fetchFlightStatusFunction = (address, flight) => new Promise((resolve, reject) => {
  flightSuretyApp.methods
    .fetchFlightStatus(address, flight, new Date().getTime())
    .send({
      from: address,
      gas: GAS
    }, (error, result) => {
      if (error) {
        console.error('ERROR: flight status, check logs.')
        reject(error)
      } else {
        resolve(result)
      }
    })
})
const fetchFlightStatus = async () => {
  const flight = 'ES1111'
  const accounts = await web3.eth.getAccounts()
  owner = accounts[0]
  const airline = accounts[1]
  if (!added) {
    await fetchFlightStatusRegister(airline)
    console.log(`AIRLINE: registered with address ${airline}`)
    await fetchFlightStatusAddFunds(airline)
    console.log(`AIRLINE: funds added from address ${airline}`)
    await fetchFlightStatusAddFlight(airline, flight)
    console.log(`FLIGHT: ${flight} added`)
    added = true
  }
  await fetchFlightStatusFunction(airline, flight)
  console.log(`FLIGHT STATUS: ${flight} status requested`)
}
//   # Do something when OracleRequest
const submitOracleResponse = (index, airline, flight, timestamp, statusCode, address) => new Promise((resolve, reject) => {
  flightSuretyApp.methods
    .submitOracleResponse(index, airline, flight, timestamp, statusCode)
    .send({
      from: address,
      gas: GAS
    }, (error, result) => {
      if (error) {
        console.error('ERROR: flight status, check logs.')
        reject(error)
      } else {
        resolve(result)
      }
    })
})
const processOracleRequest = async (event) => {
  console.log('EVENT: OracleRequest fired!')
  const {
    index,
    airline,
    flight,
    timestamp
  } = event.returnValues
  const o = []
  for (let address of oracles) {
    const indexes = await getOraclesIndexes(address)
    for (let i of indexes) {
      if (i === index) {
        console.log(`EVENT: Oracle with address ${address} has the same index.`)
        o.push(address)
      }
    }
  }
  if (o.length) {
    const i = Math.floor((Math.random() * statusCodes.length))
    const statusCode = statusCodes[i]
    for (let address of o) {
      await submitOracleResponse(index, airline, flight, timestamp, statusCode, address)
      console.log(`EVENT: Oracle with address ${address} has submited the code ${statusCode}.`)
    }
  } else {
    console.log(`EVENT: There are no oracles with the index ${index}`)
  }
}

// SERVER
const app = express()
//   # Targets
app.get('/getOracles', async (request, response) => {
  const result = []
  for (let address of oracles) {
    const values = await getOraclesIndexes(address)
    result.push({
      address,
      indexes: values
    })
  }
  response.send(result)
})
app.get('/fetchFlightStatus', async (request, response) => {
  await fetchFlightStatus()
  response.send('DONE')
})
//   # Launch the server
const registerOracles = (address) => new Promise((resolve, reject) => {
  flightSuretyApp.methods
    .registerOracle()
    .send({
      from: address,
      gas: GAS,
      value: web3.utils.toWei('1', 'ether')
    }, (error, result) => {
      if (error) {
        console.error('\tERROR: registering oracle, check logs.')
        reject(error)
      } else {
        resolve(result)
      }
    })
})
const launch = async () => {
  console.log('### FLIGHTSURETY BACKEND ###')
  console.log('Registering oracles...')
  const accounts = await web3.eth.getAccounts()
  for (let address of accounts.slice(1, ORACLE_NUMBER + 1)) {
    await registerOracles(address)
    console.log(`\tORACLE: registered with address ${address}`)
    oracles.push(address)
  }
  await wait(1000)
  console.log(`${oracles.length} oracles registered.`)
  app.listen(PORT, () => {
    console.log(`Server launched on "http://localhost:${PORT}".`)
    console.log('Available targets:')
    console.log(`\t- "http://localhost:${PORT}/getOracles": get data from registered oracles by the server.`)
    console.log(`\t- "http://localhost:${PORT}/fetchFlightStatus": simulate a fetch flight status request.`)
  })
}

// Launch the server
launch()
