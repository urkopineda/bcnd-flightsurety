pragma solidity ^0.4.24;

import "../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";

contract FlightSuretyData {
    using SafeMath for uint256;

    // VARIABLES
    //   - BASE

    address private contractOwner;
    bool private operational = true;
    mapping(address => uint256) private authorizedContracts;

    //   - AIRLINE

    struct Airline {
        address airline;
        bool status;
        uint256 funds;
    }

    mapping(address => Airline) private airlines;
    mapping(address => uint256) private airlinesConsensus;
    uint256 private airlineCount = 0;

    //   - FLIGHT

    struct Flight {
        bool isRegistered;
        uint8 statusCode;
        uint256 updatedTimestamp;
        mapping(uint8 => uint256) history;
        address airline;
    }

    mapping(string => Flight) private flights;

    //   - PASSENGERS

    struct Booking {
        bool booked;
        bool claimed;
        string flight;
        address passenger;
        uint256 value;
    }

    mapping(bytes32 => Booking) private bookings;
    mapping(address => uint256) private passengerFunds;

    //   - ORACLES

    struct Oracle {
        bool isRegistered;
        uint8[3] indexes;
    }

    struct OracleRequester {
        address requester;
        bool isOpen;
        mapping(uint8 => address[]) responses;
    }

    uint8 private nonce = 0;

    mapping(address => Oracle) private oracles;
    mapping(bytes32 => OracleRequester) private oracleResponses;

    // CONSTRUCTOR

    constructor() public payable {
        contractOwner = msg.sender;
        authorizedContracts[msg.sender] = 1;
        airlines[contractOwner] = Airline({
            airline: contractOwner,
            status: true,
            funds: 0
        });
        airlineCount = airlineCount.add(1);
    }

    // MODIFIERS
    //   - BASE

    modifier requireIsOperational() {
        require(operational, "Contract is currently not operational");
        _;
    }

    modifier requireContractOwner() {
        require(msg.sender == contractOwner, "Caller is not contract owner");
        _;
    }

    modifier requireAuthorizedContract() {
        require(authorizedContracts[msg.sender] == 1, "Caller is not an authorized contract");
        _;
    }

    // FUNCTIONS
    //   - BASE

    function authorizeContract(address addr)
    public
    requireContractOwner {
        authorizedContracts[addr] = 1;
    }

    function deauthorizeContract(address addr)
    public
    requireContractOwner {
        authorizedContracts[addr] = 0;
    }

    function isCallerAuthorized(address addr)
    public
    view
    returns (bool) {
        return authorizedContracts[addr] == 1;
    }

    function isOperational()
    public
    view
    requireAuthorizedContract
    returns (bool) {
        return operational;
    }

    function setOperatingStatus(bool mode)
    external
    requireAuthorizedContract {
        operational = mode;
    }

    //   - AIRLINE

    function registerAirline(address airlineAddress, bool status, uint256 funds)
    external
    requireAuthorizedContract {
        airlines[airlineAddress] = Airline({
            airline: airlineAddress,
            status: status,
            funds: funds
        });
        airlineCount = airlineCount.add(1);
    }

    function getAirline(address airlineAddress)
    external
    view
    requireAuthorizedContract
    returns (address airline, bool status, uint256 funds) {
        Airline memory temp = airlines[airlineAddress];
        return (temp.airline, temp.status, temp.funds);
    }

    function getAirlineCount()
    external
    view
    requireAuthorizedContract
    returns (uint256 _airlineCount) {
        return airlineCount;
    }

    function getAirlineConsensus(address airline)
    external
    view
    requireAuthorizedContract
    returns (uint256 _airlinesConsensus) {
        return airlinesConsensus[airline];
    }

    function isAirline(address airlineAddress)
    external
    view
    requireAuthorizedContract
    returns (bool status) {
        Airline memory temp = airlines[airlineAddress];
        return temp.status;
    }

    function setAirlineFunds(address airlineAddress, uint256 funds)
    external
    payable
    requireAuthorizedContract {
        airlines[airlineAddress].funds = airlines[airlineAddress].funds.add(funds);
    }

    function addAirlineConsensus(address airline)
    external
    requireAuthorizedContract
    returns (uint256 _airlinesConsensus) {
        airlinesConsensus[airline] = airlinesConsensus[airline].add(1);
        return airlinesConsensus[airline];
    }

    //   - FLIGHT

    function registerFlight(string flight, bool isRegistered, uint8 statusCode, uint256 updatedTimestamp, address airline)
    external
    requireAuthorizedContract {
        flights[flight] = Flight({
            isRegistered: isRegistered,
            statusCode: statusCode,
            updatedTimestamp: updatedTimestamp,
            airline: airline
        });
        flights[flight].history[statusCode] = updatedTimestamp;
    }

    function processFlightStatus(string flight, uint8 statusCode, uint256 updatedTimestamp)
    external
    requireAuthorizedContract {
        flights[flight].statusCode = statusCode;
        flights[flight].updatedTimestamp = updatedTimestamp;
        flights[flight].history[statusCode] = updatedTimestamp;
    }

    function getFlight(string flight)
    external
    view
    requireAuthorizedContract
    returns (bool isRegistered, uint8 statusCode, uint256 updatedTimestamp, address airline) {
        Flight memory temp = flights[flight];
        return (temp.isRegistered, temp.statusCode, temp.updatedTimestamp, temp.airline);
    }

    function isFlight(string flight)
    external
    view
    requireAuthorizedContract
    returns (bool status) {
        Flight memory temp = flights[flight];
        return (temp.isRegistered);
    }

    //   - PASSENGERS

    function bookFlight(string flight, address passenger)
    external
    payable
    requireAuthorizedContract {
        bytes32 key = keccak256(abi.encodePacked(flight, passenger));
        bookings[key] = Booking({
            booked: true,
            claimed: false,
            flight: flight,
            passenger: passenger,
            value: msg.value
        });
    }

    function getBooking(string flight, address passenger)
    external
    view
    requireAuthorizedContract
    returns (bool booked, bool claimed, string memory _flight, address _passenger, uint256 value) {
        bytes32 key = keccak256(abi.encodePacked(flight, passenger));
        Booking memory booking = bookings[key];
        return (booking.booked, booking.claimed, booking.flight, booking.passenger, booking.value);
    }

    function isBooked(string flight, address passenger)
    external
    view
    requireAuthorizedContract
    returns (bool status) {
        bytes32 key = keccak256(abi.encodePacked(flight, passenger));
        Booking memory booking = bookings[key];
        return (booking.booked);
    }

    function isClaimed(string flight, address passenger)
    external
    view
    requireAuthorizedContract
    returns (bool claimed) {
        bytes32 key = keccak256(abi.encodePacked(flight, passenger));
        Booking memory booking = bookings[key];
        return (booking.claimed);
    }

    function setClaimed(string flight, address passenger, uint256 value)
    external
    requireAuthorizedContract {
        bytes32 key = keccak256(abi.encodePacked(flight, passenger));
        bookings[key].claimed = true;
        passengerFunds[passenger] = passengerFunds[passenger].add(value);
    }

    function getFunds(address addr)
    external
    view
    requireAuthorizedContract
    returns (uint256 value) {
        return passengerFunds[addr];
    }

    function withdraw(address addr, uint256 value)
    external
    requireAuthorizedContract {
        passengerFunds[addr] = 0;
        addr.transfer(value);
    }

    //   - ORACLES

    function registerOracle(address addr)
    external
    payable
    requireAuthorizedContract {
        contractOwner.transfer(msg.value);
        uint8[3] memory indexes = generateIndexes(addr);
        oracles[addr] = Oracle({
            isRegistered: true,
            indexes: indexes
        });
    }

    function getOracleIndexes(address addr)
    external
    view
    requireAuthorizedContract
    returns (uint8[3] memory indexes) {
        return oracles[addr].indexes;
    }

    function isOracle(address addr)
    external
    view
    requireAuthorizedContract
    returns (bool status) {
        return oracles[addr].isRegistered;
    }

    function isOraclesIndex(address addr, uint8 index)
    external
    view
    requireAuthorizedContract
    returns (bool result) {
        return (
            (oracles[addr].indexes[0] == index) ||
            (oracles[addr].indexes[1] == index) ||
            (oracles[addr].indexes[2] == index)
        );
    }

    function isOracleRequested(uint8 index, address airline, string flight, uint256 timestamp)
    external
    view
    requireAuthorizedContract
    returns (bool result) {
        bytes32 key = keccak256(abi.encodePacked(index, airline, flight, timestamp));
        return oracleResponses[key].isOpen;
    }

    function generateIndexes(address account)
    internal
    returns(uint8[3] memory) {
        uint8[3] memory indexes;
        indexes[0] = getRandomIndex(account);
        indexes[1] = indexes[0];
        while (indexes[1] == indexes[0]) {
            indexes[1] = getRandomIndex(account);
        }
        indexes[2] = indexes[1];
        while ((indexes[2] == indexes[0]) || (indexes[2] == indexes[1])) {
            indexes[2] = getRandomIndex(account);
        }
        return indexes;
    }

    function getRandomIndex(address account)
    internal
    returns(uint8) {
        uint8 maxValue = 10;
        uint8 random = uint8(uint256(keccak256(abi.encodePacked(blockhash(block.number - nonce++), account))) % maxValue);
        if (nonce > 250) {
            nonce = 0;
        }
        return random;
    }

    function fetchFlightStatus(address addr, address airline, string flight, uint256 timestamp)
    external
    requireAuthorizedContract
    returns (uint8 index) {
        uint8 _index = getRandomIndex(addr);
        bytes32 key = keccak256(abi.encodePacked(_index, airline, flight, timestamp));
        oracleResponses[key] = OracleRequester({
            requester: addr,
            isOpen: true
        });
        return _index;
    }

    function submitOracleResponse(address addr, uint8 index, address airline, string flight, uint256 timestamp, uint8 statusCode)
    external
    requireAuthorizedContract
    returns (address[] responses) {
        bytes32 key = keccak256(abi.encodePacked(index, airline, flight, timestamp)); 
        oracleResponses[key].responses[statusCode].push(addr);
        return oracleResponses[key].responses[statusCode];
    }

    function() public payable {

    }

}
