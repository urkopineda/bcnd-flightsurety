pragma solidity ^0.4.24;

import "../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./FlightSuretyData.sol";

contract FlightSuretyApp {
    using SafeMath for uint256;

    // VARIABLES
    //   - BASE

    address private contractOwner;
    FlightSuretyData flightSuretyData;

    //   - AIRLINE

    uint8 private constant AIRLINE_MINIMUM_COUNT = 4;
    uint256 private constant AIRLINE_MINIMUM_FUNDS = 10000000000000000000;

    //   - FLIGHT

    uint8 private constant STATUS_CODE_UNKNOWN = 0;
    uint8 private constant STATUS_CODE_ON_TIME = 10;
    uint8 private constant STATUS_CODE_LATE_AIRLINE = 20;
    uint8 private constant STATUS_CODE_LATE_WEATHER = 30;
    uint8 private constant STATUS_CODE_LATE_TECHNICAL = 40;
    uint8 private constant STATUS_CODE_LATE_OTHER = 50;

    //   - PASSENGERS

    uint256 private constant PASSENGERS_MAXIMUM_INSURANCE = 1000000000000000000;

    //   - ORACLES

    uint256 public constant REGISTRATION_FEE = 1 ether;
    uint256 private constant MIN_RESPONSES = 3;

    // EVENTS

    event AirlineRegistered(address airline);
    event FlightRegistered(address airline, string flight);
    event FlightBooked(address passenger, string flight);
    event BookingClaimed(address passenger, string flight, bool result, uint256 returnings);
    event FundsWithdrawed(address passenger, uint256 value);
    event FlightStatusInfo(address airline, string flight, uint256 timestamp, uint8 status);
    event OracleReport(address airline, string flight, uint256 timestamp, uint8 status);
    event OracleRequest(uint8 index, address airline, string flight, uint256 timestamp);

    // CONSTRUCTOR

    constructor(address dataContract)
    public
    payable {
        contractOwner = msg.sender;
        flightSuretyData = FlightSuretyData(dataContract);
    }

    // MODIFIERS
    //   - BASE

    modifier requireIsOperational() {
        require(flightSuretyData.isOperational() == true, "Contract is currently not operational");
        _;
    }

    modifier requireContractOwner() {
        require(msg.sender == contractOwner, "Caller is not contract owner");
        _;
    }

    modifier requirePayable() {
        require(msg.value > 0, "Value is not valid");
        _;
    }

    modifier requireAirline() {
        require(flightSuretyData.isAirline(msg.sender) == true, "Your must be an active airline.");
        _;
    }
    modifier requireNotAirline(address airline) {
        require(flightSuretyData.isAirline(airline) == false, "Airline already registered.");
        _;
    }

    modifier requireAirlineMinimumFunds() {
        (, , uint256 funds) = flightSuretyData.getAirline(msg.sender);
        require(funds >= AIRLINE_MINIMUM_FUNDS, "Your funds must be, at least, 10 ether.");
        _;
    }

    modifier requireFlight(string flight) {
        require(flightSuretyData.isFlight(flight) == true, "This flight not exists");
        _;
    }

    modifier requireNotFlight(string flight) {
        require(flightSuretyData.isFlight(flight) == false, "This flight already exists");
        _;
    }

    modifier requireMaximumInsurance() {
        require(msg.value <= PASSENGERS_MAXIMUM_INSURANCE, "You have reached maximum insurance limit.");
        _;
    }

    modifier requireBooked(string flight) {
        require(flightSuretyData.isBooked(flight, msg.sender) == true, "You have not booked this flight.");
        _;
    }

    modifier requireNotBooked(string flight) {
        require(flightSuretyData.isBooked(flight, msg.sender) == false, "You have booked this flight already.");
        _;
    }

    modifier requireNotClaimed(string flight) {
        require(flightSuretyData.isClaimed(flight, msg.sender) == false, "This flight is already claimed");
        _;
    }

    modifier requireFunds() {
        require(flightSuretyData.getFunds(msg.sender) > 0, "You have no funds to withdraw.");
        _;
    }

    modifier requireFee() {
        require(msg.value >= REGISTRATION_FEE, "Registration fee is required");
        _;
    }

    modifier requireOracle() {
        require(flightSuretyData.isOracle(msg.sender) == true, "This oracle does not exists");
        _;
    }

    modifier requireNotOracle() {
        require(flightSuretyData.isOracle(msg.sender) == false, "This oracle already exists");
        _;
    }

    modifier requireIsIndexOracle(uint8 index) {
        require(flightSuretyData.isOraclesIndex(msg.sender, index) == true, "Index does not match oracle request");
        _;
    }

    modifier requireIsRequestedOracle(uint8 index, address airline, string flight, uint256 timestamp) {
        require(flightSuretyData.isOracleRequested(index, airline, flight, timestamp) == true, "Flight or timestamp do not match oracle request");
        _;
    }

    // FUNCTIONS
    //   - BASE

    function isOperational()
    public
    view
    returns (bool status) {
        return flightSuretyData.isOperational();
    }

    function setOperatingStatus(bool mode)
    public
    requireContractOwner {
        flightSuretyData.setOperatingStatus(mode);
    }

    //   - AIRLINE

    function registerAirline(address newAirline)
    external
    requireAirline
    requireNotAirline(newAirline)
    returns (bool success, uint256 votes) {
        bool status = false;
        uint256 consensus = 0;
        uint256 neededVotes = 0;
        uint256 airlineCount = flightSuretyData.getAirlineCount();
        if (airlineCount >= AIRLINE_MINIMUM_COUNT) {
            consensus = flightSuretyData.addAirlineConsensus(newAirline);
            neededVotes = airlineCount.div(2);
        }
        if (consensus >= neededVotes) {
            status = true;
            flightSuretyData.registerAirline(newAirline, status, 0);
            emit AirlineRegistered(newAirline);
        }
        return (status, consensus);
    }

    function addFunds()
    external
    payable
    requirePayable
    requireAirline {
        flightSuretyData.setAirlineFunds.value(msg.value)(msg.sender, msg.value);
    }

    //   - FLIGHT

    function registerFlight(string flight)
    external
    requireAirline
    requireAirlineMinimumFunds
    requireNotFlight(flight) {
        flightSuretyData.registerFlight(
            flight,
            true,
            STATUS_CODE_UNKNOWN,
            now,
            msg.sender
        );
        emit FlightRegistered(msg.sender, flight);
    }

    //   - PASSENGERS

    function bookFlight(string flight)
    external
    payable
    requirePayable
    requireMaximumInsurance 
    requireFlight(flight)
    requireNotBooked(flight) {
        flightSuretyData.bookFlight.value(msg.value)(flight, msg.sender);
        emit FlightBooked(msg.sender, flight);
    }

    function claim(string flight)
    external
    requireFlight(flight)
    requireBooked(flight)
    requireNotClaimed(flight) {
        bool _result = false;
        uint256 _returnings = 0;
        (, uint8 statusCode, , ) = flightSuretyData.getFlight(flight);
        if (statusCode == STATUS_CODE_LATE_AIRLINE) {
            (, , , , uint256 value) = flightSuretyData.getBooking(flight, msg.sender);
            _result = true;
            _returnings = value.mul(15);
            _returnings = _returnings.div(10);
            flightSuretyData.setClaimed(flight, msg.sender, _returnings);
        }
        emit BookingClaimed(msg.sender, flight, _result, _returnings);
    }

    function withdraw()
    external
    requireFunds {
        uint256 value = flightSuretyData.getFunds(msg.sender);
        flightSuretyData.withdraw(msg.sender, value);
        emit FundsWithdrawed(msg.sender, value);
    }

    //   - ORACLES

    function registerOracle()
    external
    payable
    requireFee
    requireNotOracle {
        flightSuretyData.registerOracle.value(msg.value)(msg.sender);
    }

    function getOracleIndexes()
    external
    view
    requireOracle
    returns (uint8[3] memory indexes) {
        return flightSuretyData.getOracleIndexes(msg.sender);
    }

    function fetchFlightStatus(address airline, string flight, uint256 timestamp)
    requireFlight(flight)
    external {
        uint8 index = flightSuretyData.fetchFlightStatus(msg.sender, airline, flight, timestamp);
        emit OracleRequest(index, airline, flight, timestamp);
    }

    function submitOracleResponse(uint8 index, address airline, string flight, uint256 timestamp, uint8 statusCode)
    external
    requireIsIndexOracle(index)
    requireIsRequestedOracle(index, airline, flight, timestamp) {
        address[] memory responses = flightSuretyData.submitOracleResponse(msg.sender, index, airline, flight, timestamp, statusCode);
        emit OracleReport(airline, flight, timestamp, statusCode);
        if (responses.length >= MIN_RESPONSES) {
            flightSuretyData.processFlightStatus(flight, statusCode, timestamp);
            emit FlightStatusInfo(airline, flight, timestamp, statusCode);
        }
    }

    function() public payable {

    }
}
