# FlightSurety

FlightSurety is a dapp in which airlines and passengers could manage the insurance booking.

> NOTE: There are some screenshots with the server, dapp and test working in the `./screenshots` directory:
> * `dapp-1.png` shows the dapp just executed.
> * `dapp-2.png` shows the dapp with the logs when a user successfully claimed.
> * `server.png` shows the server oracles working.
> * `test.png` shows all test execution.

## First steps

You should have installed `node` and `npm`. Then, run on the root directory `yarn`.

## Testing

To launch the test, open two terminals.

1. First, run `yarn ganache` on the first one.
    * Check the ports and modify the `truffle.js` file if needed.
2. Then, run `yarn test:full` on the second one to test the contracts and oracles.

## Launching apps

To launch the apps (server and dapp) follow the next steps

### Server

The server is an Express.js server, check `./src/server.js` file.

To launch the server, open two terminals.

1. First, run `yarn ganache` on the first one.
    * Check the ports and modify the `truffle.js` file if needed.
2. Then, run `yarn server` on the second one, or `yarn server:full` if you didn't compile and migrate yet.
    * Oracles are registered at start.
    * Request `/getOracles` to check the indexes.
    * If you want to simulate a flight status info request, request `\flightStatusInfo`.

The server will run on `http://localhost:3000`.

> NOTE: Logs are very informative, check them!

### Dapp

The dapp is a React application, check the directory `./src/dapp` directory.

With the server launched, to launch the dapp, use the command `yarn start` or `yarn dapp`, or `yarn dapp:full` if you didn't compile and migrate yet. Go to `http://localhost:8000` to check it out.

To build a production release, use `yarn dapp:prod`and check the `./dapp` directory.
