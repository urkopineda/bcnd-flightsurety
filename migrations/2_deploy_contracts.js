const FlightSuretyApp = artifacts.require('FlightSuretyApp');
const FlightSuretyData = artifacts.require('FlightSuretyData');
const fs = require('fs');

module.exports = (deployer, network, accounts) => {
  const owner = accounts[0]
  deployer.deploy(FlightSuretyData)
    .then(() => deployer.deploy(FlightSuretyApp, FlightSuretyData.address)
      .then(async () => {
        const config = {
          localhost: {
            url: 'http://localhost:8545',
            dataAddress: FlightSuretyData.address,
            appAddress: FlightSuretyApp.address
          }
        }
        const data = await FlightSuretyData.deployed()
        const address = await FlightSuretyApp.address
        // authorize address of app contract
        await data.authorizeContract(address, {from: owner})
        // writhe configuration files
        await fs.writeFileSync(`${__dirname}/../src/config.json`, JSON.stringify(config, null, '\t'), 'utf-8');
        await fs.writeFileSync(`${__dirname}/../src/dapp/src/config.json`, JSON.stringify(config, null, '\t'), 'utf-8');
      })
  )
}