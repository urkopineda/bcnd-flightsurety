require('./flightSurety')

const FlightSuretyApp = artifacts.require('FlightSuretyApp')
const FlightSuretyData = artifacts.require('FlightSuretyData')
const BigNumber = require('bignumber.js')
const ganache = require('ganache-cli')
const Web3 = require('web3')
const web3 = new Web3(ganache.provider())

contract('Oracles', async (accounts) => {

  const STATUS_CODE_UNKNOWN = 0
  const STATUS_CODE_ON_TIME = 10

  const owner = accounts[0]
  const oracles = []
  const airline = accounts[0]
  const flight = 'ES1010'
  const notFlight = 'ES0000'
  const timestamp = new Date().getTime()

  for (let i = 1; i <= 20; i++) {
    oracles.push(accounts[i])
  }

  const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

  before(async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    const address = await FlightSuretyApp.address
    // authorize address of app contract
    await data.authorizeContract(address, {from: owner})
    // add funds
    await contract.addFunds({from: airline, value: web3.utils.toWei('10', 'ether')})
    // register flight
    await contract.registerFlight(flight, {from: airline})
  })

  it('(oracles) address can\'t register a oracle without paying fee', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it faile because of fee
    let failure = false
    try {
      await contract.registerOracle({from: oracles[1], value: web3.utils.toWei('0.9', 'ether')})
    } catch (error) {
      failure = true
    }
    // assert failure
    assert.equal(failure, true, 'should fail because of fee')
  })

  it('(oracles) addresses can register oracles', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // register the oracles
    for (let address of oracles) {
      await contract.registerOracle({from: address, value: web3.utils.toWei('1', 'ether')})
      // check that is registered
      const status = await data.isOracle(address, {from: owner})
      // assert status
      assert.equal(status, true, 'should be registered')
    }
  })

  it('(oracles) address can\'t register the same oracle twice', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it faile because already exists
    let failure = false
    try {
      await contract.registerOracle({from: oracles[1], value: web3.utils.toWei('1', 'ether')})
    } catch (error) {
      failure = true
    }
    // assert failure
    assert.equal(failure, true, 'should fail because already exists')
  })

  it('(oracles) address can get indexes being an oracle', async () => {
    const contract = await FlightSuretyApp.deployed()
    // get indexes
    const indexes = await contract.getOracleIndexes({from: oracles[0]})
    const parsedIndexes = []
    for (let index of indexes) {
      parsedIndexes.push(BigNumber(index).toNumber())
    }
    // assert type of indexes
    assert.equal(parsedIndexes.length, 3, 'indexes should be 3')
    assert.equal(typeof parsedIndexes[0], typeof 1, 'indexes should be int')
    assert.equal(typeof parsedIndexes[1], typeof 1, 'indexes should be int')
    assert.equal(typeof parsedIndexes[2], typeof 1, 'indexes should be int')
  })

  it('(oracles) address can\'t ask for fetching flight status if flight does not exists', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it fail asking for fetching flight status when flight does not exists
    let failure = false
    try {
      await contract.fetchFlightStatus(airline, notFlight, timestamp, {from: owner})
    } catch (error) {
      failure = true
    }
    // assert failure
    assert.equal(failure, true, 'fails because flight does not exists')
  })

  it('(oracles) address can ask for fetching flight status and index exists', async () => {
    const contract = await FlightSuretyApp.deployed()
    // watch the emitted event
    let event = false
    let _index
    let _airline
    let _flight
    let _timestamp
    await contract.OracleRequest({}, (error, result) => {
      if (!error) {
        event = true
        _index = BigNumber(result.args.index).toNumber()
        _airline = result.args.airline
        _flight = result.args.flight
        _timestamp = result.args.timestamp
      }
    })
    // ask for fetching flight status
    await contract.fetchFlightStatus(airline, flight, timestamp, {from: owner})
    // wait for the event to fire
    await wait(1000)
    // assert events and data
    assert.equal(event, true, 'event fired correctly')
    assert.equal(typeof _index, typeof 1, 'typeof index is correct')
    assert.equal(_airline, airline, 'airlines are equal')
    assert.equal(_flight, flight, 'flights are equal')
    assert.equal(_timestamp, timestamp, 'timestamps are equal')
    // check that an oracle has the index requested
    let exists = false
    for (let address of oracles) {
      const indexes = await contract.getOracleIndexes({from: address})
      const parsedIndexes = []
      for (let index of indexes) {
        parsedIndexes.push(BigNumber(index).toNumber())
      }
      if (parsedIndexes.includes(_index)) {
        exists = true
      }
    }
    // assert that exists
    assert.equal(exists, true, 'an oracle has the index')
  })

  it('(oracles) oracle submiting fails because index is incorrect', async () => {
    const contract = await FlightSuretyApp.deployed()
    // get indexes
    const indexes = await contract.getOracleIndexes({from: oracles[0]})
    // transform index to make it fail
    const index = BigNumber(indexes[0]).toNumber() + 1
    // make it fail
    let failure = false
    try {
      await contract.submitOracleResponse(index, airline, flight, timestamp, STATUS_CODE_ON_TIME)
    } catch (error) {
      failure = true
    }
    // assert failure
    assert.equal(failure, true, 'fails because of altered index')
  })

  it('(oracles) oracle submiting fails because it is not the oracle requested', async () => {
    const contract = await FlightSuretyApp.deployed()
    // watch the emitted event
    let event = false
    let _index
    let _airline
    let _flight
    let _timestamp
    await contract.OracleRequest({}, (error, result) => {
      if (!error) {
        event = true
        _index = BigNumber(result.args.index).toNumber()
        _airline = result.args.airline
        _flight = result.args.flight
        _timestamp = result.args.timestamp
      }
    })
    // ask for fetching flight status
    await contract.fetchFlightStatus(airline, flight, timestamp, {from: owner})
    // wait for the event to fire
    await wait(1000)
    // assert events and data
    assert.equal(event, true, 'event fired correctly')
    assert.equal(typeof _index, typeof 1, 'typeof index is correct')
    assert.equal(_airline, airline, 'airlines are equal')
    assert.equal(_flight, flight, 'flights are equal')
    assert.equal(_timestamp, timestamp, 'timestamps are equal')
    // check that an oracle has the index requested
    let o = oracles[0]
    for (let address of oracles) {
      const indexes = await contract.getOracleIndexes({from: address})
      const parsedIndexes = []
      for (let index of indexes) {
        parsedIndexes.push(BigNumber(index).toNumber())
      }
      if (!parsedIndexes.includes(_index)) {
        o = address
      }
    }
    // get indexes
    const indexes = await contract.getOracleIndexes({from: o})
    // transform index to make it fail
    const index = BigNumber(indexes[0]).toNumber()
    // make it fail
    let failure = false
    try {
      await contract.submitOracleResponse(index, airline, flight, timestamp, STATUS_CODE_ON_TIME)
    } catch (error) {
      failure = true
    }
    // assert failure
    assert.equal(failure, true, 'fails because is not the oracle requested')
  })

  it('(oracles) oracle submiting fails because minimum reports to make the status change is 3', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // watch the emitted event request
    let eventRequest = false
    let eventRequestIndex
    let eventRequestAirline
    let eventRequestFlight
    let eventRequestTimestamp
    await contract.OracleRequest({}, (error, result) => {
      if (!error) {
        eventRequest = true
        eventRequestIndex = BigNumber(result.args.index).toNumber()
        eventRequestAirline = result.args.airline
        eventRequestFlight = result.args.flight
        eventRequestTimestamp = result.args.timestamp
      }
    })
    // ask for fetching flight status
    await contract.fetchFlightStatus(airline, flight, timestamp, {from: owner})
    // wait for the event to fire
    await wait(1000)
    // assert events and data
    assert.equal(eventRequest, true, 'event fired correctly')
    assert.equal(typeof eventRequestIndex, typeof 1, 'typeof index is correct')
    assert.equal(eventRequestAirline, airline, 'airlines are equal')
    assert.equal(eventRequestFlight, flight, 'flights are equal')
    assert.equal(eventRequestTimestamp, timestamp, 'timestamps are equal')
    // check that an oracle has the index requested
    let exists = false
    let o
    for (let address of oracles) {
      const indexes = await contract.getOracleIndexes({from: address})
      const parsedIndexes = []
      for (let index of indexes) {
        parsedIndexes.push(BigNumber(index).toNumber())
      }
      if (parsedIndexes.includes(eventRequestIndex)) {
        exists = true
        o = address
      }
    }
    // assert that exists
    assert.equal(exists, true, 'an oracle has the index')
    // watch the emitted event report
    let eventReport = false
    let eventReportAirline
    let eventReportFlight
    let eventReportStatusCode
    let eventReportTimestamp
    await contract.OracleReport({}, (error, result) => {
      if (!error) {
        eventReport = true
        eventReportAirline = result.args.airline
        eventReportFlight = result.args.flight
        eventReportStatusCode = BigNumber(result.args.status).toNumber()
        eventReportTimestamp = result.args.timestamp
      }
    })
    // watch the emitted event flight status info
    let eventFlightStatusInfo = false
    await contract.FlightStatusInfo({}, (error) => {
      if (!error) {
        eventFlightStatusInfo = true
      }
    })
    // submit a response
    await contract.submitOracleResponse(eventRequestIndex, airline, flight, timestamp, STATUS_CODE_ON_TIME, {from: o})
    // wait for the event to fire
    await wait(1000)
    // get flight status code
    const values = await data.getFlight(flight, {from: owner})
    const statusCode = BigNumber(values[1]).toNumber()
    // assert failure
    assert.equal(statusCode, STATUS_CODE_UNKNOWN, 'the status code is unknown, not changed')
    assert.equal(eventReport, true, 'event report fired')
    assert.equal(eventReportAirline, airline, 'event report airline is correct')
    assert.equal(eventReportFlight, flight, 'event report flight is correct')
    assert.equal(eventReportStatusCode, STATUS_CODE_ON_TIME, 'event report code is correct')
    assert.equal(eventReportTimestamp, timestamp, 'event report timestamp is correct')
    assert.equal(eventFlightStatusInfo, false, 'event flight status info not fired')
  })

  it('(oracles) oracle submiting works correctly', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // watch the emitted event request
    let eventRequest = false
    let eventRequestIndex
    let eventRequestAirline
    let eventRequestFlight
    let eventRequestTimestamp
    await contract.OracleRequest({}, (error, result) => {
      if (!error) {
        eventRequest = true
        eventRequestIndex = BigNumber(result.args.index).toNumber()
        eventRequestAirline = result.args.airline
        eventRequestFlight = result.args.flight
        eventRequestTimestamp = result.args.timestamp
      }
    })
    // ask for fetching flight status
    await contract.fetchFlightStatus(airline, flight, timestamp, {from: owner})
    // wait for the event to fire
    await wait(1000)
    // assert events and data
    assert.equal(eventRequest, true, 'event fired correctly')
    assert.equal(typeof eventRequestIndex, typeof 1, 'typeof index is correct')
    assert.equal(eventRequestAirline, airline, 'airlines are equal')
    assert.equal(eventRequestFlight, flight, 'flights are equal')
    assert.equal(eventRequestTimestamp, timestamp, 'timestamps are equal')
    // check that an oracle has the index requested
    let exists = false
    let o
    for (let address of oracles) {
      const indexes = await contract.getOracleIndexes({from: address})
      const parsedIndexes = []
      for (let index of indexes) {
        parsedIndexes.push(BigNumber(index).toNumber())
      }
      if (parsedIndexes.includes(eventRequestIndex)) {
        exists = true
        o = address
      }
    }
    // assert that exists
    assert.equal(exists, true, 'an oracle has the index')
    // watch the emitted event flight status info
    let eventFlightStatusInfo = false
    let eventFlightStatusInfoAirline
    let eventFlightStatusInfoFlight
    let eventFlightStatusInfoTimestamp
    let eventFlightStatusInfoStatus
    await contract.FlightStatusInfo({}, (error, result) => {
      if (!error) {
        eventFlightStatusInfo = true
        eventFlightStatusInfoAirline = result.args.airline
        eventFlightStatusInfoFlight = result.args.flight
        eventFlightStatusInfoTimestamp = result.args.timestamp
        eventFlightStatusInfoStatus = BigNumber(result.args.status).toNumber()
      }
    })
    // we have to do it twice more
    for (let nothing of [...new Array(3)]) {
      // watch the emitted event report
      let eventReport = false
      let eventReportAirline
      let eventReportFlight
      let eventReportStatus
      let eventReportTimestamp
      await contract.OracleReport({}, (error, result) => {
        if (!error) {
          eventReport = true
          eventReportAirline = result.args.airline
          eventReportFlight = result.args.flight
          eventReportStatus = BigNumber(result.args.status).toNumber()
          eventReportTimestamp = result.args.timestamp
        }
      })
      // submit a response
      await contract.submitOracleResponse(eventRequestIndex, airline, flight, timestamp, STATUS_CODE_ON_TIME, {from: o})
      // wait for the event to fire
      await wait(1000)
      // assert failure
      assert.equal(eventReport, true, 'event report fired')
      assert.equal(eventReportAirline, airline, 'event report airline is correct')
      assert.equal(eventReportFlight, flight, 'event report flight is correct')
      assert.equal(eventReportStatus, STATUS_CODE_ON_TIME, 'event report code is correct')
      assert.equal(eventReportTimestamp, timestamp, 'event report timestamp is correct')
    }
    // get flight status code
    const values = await data.getFlight(flight, {from: owner})
    const statusCode = BigNumber(values[1]).toNumber()
    // assert event
    assert.equal(statusCode, STATUS_CODE_ON_TIME, 'the status code is on time')
    assert.equal(eventFlightStatusInfo, true, 'event flight status info is fired')
    assert.equal(eventFlightStatusInfoAirline, airline, 'event flight status airline is correct')
    assert.equal(eventFlightStatusInfoFlight, flight, 'event flight status flight is correct')
    assert.equal(eventFlightStatusInfoStatus, STATUS_CODE_ON_TIME, 'event flight status code is correct')
    assert.equal(eventFlightStatusInfoTimestamp, timestamp, 'event flight status timestamp is correct')
  })

})
