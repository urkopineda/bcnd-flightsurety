
const FlightSuretyApp = artifacts.require('FlightSuretyApp')
const FlightSuretyData = artifacts.require('FlightSuretyData')
const BigNumber = require('bignumber.js')
const ganache = require('ganache-cli')
const Web3 = require('web3')
const web3 = new Web3(ganache.provider())

contract('FlightSurety', async (accounts) => {

  const testAddresses = [
    '0x69e1CB5cFcA8A311586e3406ed0301C06fb839a2',
    '0xF014343BDFFbED8660A9d8721deC985126f189F3',
    '0x0E79EDbD6A727CfeE09A2b1d0A59F7752d5bf7C9',
    '0x9bC1169Ca09555bf2721A5C9eC6D69c8073bfeB4',
    '0xa23eAEf02F9E0338EEcDa8Fdd0A73aDD781b2A86',
    '0x6b85cc8f612d5457d49775439335f83e12b8cfde',
    '0xcbd22ff1ded1423fbc24a7af2148745878800024',
    '0xc257274276a4e539741ca11b590b9447b26a8051',
    '0x2f2899d6d35b1a48a4fbdc93a37a72f264a9fca7'
  ]

  const STATUS_CODE_ON_TIME = 10
  const STATUS_CODE_LATE_AIRLINE = 20
  const STATUS_CODE_LATE_WEATHER = 30
  const STATUS_CODE_LATE_TECHNICAL = 40
  const STATUS_CODE_LATE_OTHER = 50

  const owner = accounts[0]
  const airline1 = accounts[0]
  const airline2 = accounts[1]
  const airline3 = accounts[2]
  const airline4 = accounts[3]
  const airline5 = accounts[4]
  const user1 = accounts[5]
  const user2 = accounts[6]
  const flight1 = 'ES1010'
  const flight2 = 'ES2020'
  const flight3 = 'ES3030'
  const flight4 = 'ES4040'
  const insurancePayment = '1'
  const insuranceReturnings = '1.5'

  const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

  it('(base) can authorize a contract', async () => {
    const contract = await FlightSuretyData.deployed()
    const address = await FlightSuretyApp.address
    // authorize address of app contract
    await contract.authorizeContract(address, {from: owner})
    // check status
    const status = await contract.isCallerAuthorized(address, {from: owner})
    // assert status
    assert.equal(status, true, 'incorrect status for an authorized contract')
  })

  it('(base) can deauthorize a contract', async () => {
    const contract = await FlightSuretyData.deployed()
    const address = await FlightSuretyApp.address
    // authorize address of app contract
    await contract.deauthorizeContract(address, {from: owner})
    // check status
    const status = await contract.isCallerAuthorized(address, {from: owner})
    // assert status
    assert.equal(status, false, 'incorrect status for an deauthorized contract')
    // authorize back
    await contract.authorizeContract(address, {from: owner})
  })

  it('(base) has correct initial operational value', async () => {
    const contract = await FlightSuretyApp.deployed()
    // get status
    const status = await contract.isOperational({from: owner})
    // assert status
    assert.equal(status, true, 'incorrect initial operating status value')
  })

  it('(base) can block access to operational for non-contract owner account', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it fail to change operating status
    let accessDenied = false
    try {
      await contract.setOperatingStatus(false, { from: testAddresses[2] })
    } catch(error) {
      accessDenied = true
    }
    // assert fail
    assert.equal(accessDenied, true, 'access not restricted to contract owner')
  })

  it('(base) can allow access operational for contract owner account', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it work to change operating status
    let accessDenied = false
    try {
      await contract.setOperatingStatus(false)
    } catch(error) {
        accessDenied = true
    }
    // assert works
    assert.equal(accessDenied, false, 'access not restricted to contract owner')
  })

  it('(base) can block access to functions using operational when operating status is false', async () => {
    const contract = await FlightSuretyApp.deployed()
    // change status to false
    await contract.setOperatingStatus(false)
    // now, make it fail
    let reverted = false
    try {
      await contract.setTestingMode(true)
    } catch(error) {
      reverted = true
    }
    // assert fails
    assert.equal(reverted, true, 'access not blocked')
    // revert changes
    await contract.setOperatingStatus(true)
  })

  it('(airline) only existing airline may register a new airline until there are at least four airlines registered', async () => {
    const contract = await FlightSuretyApp.deployed()
    // watch the emitted event
    let event = false
    await contract.AirlineRegistered({}, (error, result) => {
      if (!error) {
        event = true
      }
    })
    // register the next two
    await contract.registerAirline(airline2, {from: airline1})
    await contract.registerAirline(airline3, {from: airline2})
    // make it fail registering an airline without been an airline when count is bellow 4
    let fails = false
    try {
      await contract.registerAirline(airline4, {from: airline4})
    } catch (error) {
      fails = true
    }
    // wait for the event to fire
    await wait(1000)
    // assert fail
    assert.equal(fails, true, 'airline adding should fail because it is not a airline')
    assert.equal(event, true, 'event fired')
    // add the last one
    await contract.registerAirline(airline4, {from: airline3})
  })

  it('(airline) when more than four airlines, 50% of votes are needed', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // watch the emitted event
    let event = false
    let airline
    await contract.AirlineRegistered({}, (error, result) => {
      if (!error) {
        event = true
        airline = result.args.airline
      }
    })
    // register the votes (1 of 4)
    await contract.registerAirline(airline5, {from: airline1})
    // check that airline isn't a registered airline yet
    const firstStatus = await data.isAirline(airline5, {from: owner})
    // assert false
    assert.equal(firstStatus, false, 'airline should not be an airline yet (1 of 4 votes only)')
    // add the needed vote (2 of 4)
    await contract.registerAirline(airline5, {from: airline2})
    // check that airline is a registered airline
    const lastStatus = await data.isAirline(airline5, {from: owner})
    // wait for the event to fire
    await wait(1000)
    // assert true and event
    assert.equal(lastStatus, true, 'airline should be an airline (2 of 4 votes and up)')
    assert.equal(event, true, 'event fired')
    assert.equal(airline, airline5, 'event fired airline is correct')
  })

  it('(flight) airline can\'t register a flight without giving minimum funds of 10 ether', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // add funds to the contract
    await contract.addFunds({from: airline1, value: web3.utils.toWei('9', 'ether')})
    // make it fail, because it does not have minimum funds
    let fails = false
    try {
      await contract.registerFlight(flight1, {from: airline1})
    } catch(error) {
      fails = true
    }
    // assert the failure
    assert.equal(fails, true, 'airline must have 10 ether minimum funds')
    // get status of flight
    const status = await data.isFlight(flight1, {from: owner})
    // assert status
    assert.equal(status, false, 'airline has registered the flight correctly')
  })

  it('(flight) airline can register a flight after giving minimum funds of 10 ether', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // watch the emitted event
    let event = false
    let airline
    let flight
    await contract.FlightRegistered({}, (error, result) => {
      if (!error) {
        event = true
        airline = result.args.airline
        flight = result.args.flight
      }
    })
    // add funds to the contract
    await contract.addFunds({from: airline1, value: web3.utils.toWei(insurancePayment, 'ether')})
    // make it fail, because it does not have minimum funds
    let fails = false
    try {
      await contract.registerFlight(flight1, {from: airline1})
    } catch(error) {
      fails = true
    }
    // assert the failure
    assert.equal(fails, false, 'airline must have 10 ether minimum funds')
    // get status of flight
    const status = await data.isFlight(flight1, {from: owner})
    // wait for the event to fire
    await wait(1000)
    // assert status
    assert.equal(status, true, 'airline has registered the flight correctly')
    assert.equal(event, true, 'event fired')
    assert.equal(airline, airline1, 'event fired airline is correct')
    assert.equal(flight, flight1, 'event fired airline is correct')
  })

  it('(flight) airline can\'t register the same flight twice', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it fail, because exists
    let fails = false
    try {
      await contract.registerFlight(flight1, {from: airline1})
    } catch(error) {
      fails = true
    }
    // assert the failure
    assert.equal(fails, true, 'flight is already registered')
  })

  it('(passenger) passenger can\'t book a not existing flight', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it fail, because not exists
    let fails = false
    try {
      await contract.bookFlight(flight2, {from: user1, value: web3.utils.toWei(insurancePayment, 'ether')})
    } catch(error) {
      fails = true
    }
    // assert the failure
    assert.equal(fails, true, 'flight is not registered')
  })

  it('(passenger) passenger can\'t book a flight without giving any value', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it fail, because value is 0
    let fails = false
    try {
      await contract.bookFlight(flight1, {from: user1, value: 0})
    } catch(error) {
      fails = true
    }
    // assert the failure
    assert.equal(fails, true, 'value is 0')
  })

  it('(passenger) passenger can\'t book a flight if the value is greater than the maximum insurance', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it fail, because value is greater than 1
    let fails = false
    try {
      await contract.bookFlight(flight1, {from: user1, value: web3.utils.toWei('2', 'ether')})
    } catch(error) {
      fails = true
    }
    // assert the failure
    assert.equal(fails, true, 'value is greater than 1')
  })

  it('(passenger) passenger can book a flight', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // watch the emitted event
    let event = false
    let passenger
    let flight
    await contract.FlightBooked({}, (error, result) => {
      if (!error) {
        event = true
        passenger = result.args.passenger
        flight = result.args.flight
      }
    })
    // book a flight
    await contract.bookFlight(flight1, {from: user1, value: web3.utils.toWei(insurancePayment, 'ether')})
    // get status of the booking
    const status = await data.isBooked(flight1, user1, {from: owner})
    // wait for the event to fire
    await wait(1000)
    // assert status
    assert.equal(status, true, 'user has booked the flight')
    assert.equal(event, true, 'event fired')
    assert.equal(passenger, user1, 'event fired passenger is correct')
    assert.equal(flight, flight1, 'event fired airline is correct')
  })

  it('(passenger) passenger can\'t book a flight twice', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it fail, because already booked
    let fails = false
    try {
      await contract.bookFlight(flight1, {from: user1, value: web3.utils.toWei(insurancePayment, 'ether')})
    } catch(error) {
      fails = true
    }
    // assert the failure
    assert.equal(fails, true, 'already booked')
  })

  it('(claim) passenger can\'t claim a non existing flight', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it fail, because not exists
    let fails = false
    try {
      await contract.claim(flight2, {from: user1})
    } catch(error) {
      fails = true
    }
    // assert the failure
    assert.equal(fails, true, 'flight doesn\'t exist')
  })

  it('(claim) passenger can\'t claim a non booked flight', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it fail, because not booked
    let fails = false
    try {
      await contract.claim(flight1, {from: user2})
    } catch(error) {
      fails = true
    }
    // assert the failure
    assert.equal(fails, true, 'not booked')
  })

  it('(claim) passenger can\'t claim if flight status is unknown', async () => {
    const contract = await FlightSuretyApp.deployed()
    // watch the emitted event
    let event = false
    let passenger
    let flight
    let res
    let returnings
    await contract.BookingClaimed({}, (error, result) => {
      if (!error) {
        event = true
        passenger = result.args.passenger
        flight = result.args.flight
        res = result.args.result
        returnings = BigNumber(result.args.returnings)
      }
    })
    // try to claim
    await contract.claim(flight1, {from: user1})
    // wait for the event to fire
    await wait(1000)
    // assert status
    assert.equal(event, true, 'event fired')
    assert.equal(passenger, user1, 'event fired passenger is correct')
    assert.equal(flight, flight1, 'event fired airline is correct')
    assert.equal(res, false, 'event fired result is correct')
    assert.equal(returnings, 0, 'event fired returnings is correct')
  })

  it('(claim) passenger can\'t claim if flight status is on time', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // set status
    await data.processFlightStatus(flight1, STATUS_CODE_ON_TIME, new Date().getTime(), {from: owner})
    // watch the emitted event
    let event = false
    let passenger
    let flight
    let res
    let returnings
    await contract.BookingClaimed({}, (error, result) => {
      if (!error) {
        event = true
        passenger = result.args.passenger
        flight = result.args.flight
        res = result.args.result
        returnings = BigNumber(result.args.returnings)
      }
    })
    // try to claim
    await contract.claim(flight1, {from: user1})
    // wait for the event to fire
    await wait(1000)
    // assert status
    assert.equal(event, true, 'event fired')
    assert.equal(passenger, user1, 'event fired passenger is correct')
    assert.equal(flight, flight1, 'event fired airline is correct')
    assert.equal(res, false, 'event fired result is correct')
    assert.equal(returnings, 0, 'event fired returnings is correct')
  })

  it('(claim) passenger can\'t claim if flight status is late - weather', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // register another flight
    await contract.registerFlight(flight2, {from: airline1})
    // book another flight
    await contract.bookFlight(flight2, {from: user1, value: web3.utils.toWei(insurancePayment, 'ether')})
    // set status
    await data.processFlightStatus(flight2, STATUS_CODE_LATE_WEATHER, new Date().getTime(), {from: owner})
    // watch the emitted event
    let event = false
    let passenger
    let flight
    let res
    let returnings
    await contract.BookingClaimed({}, (error, result) => {
      if (!error) {
        event = true
        passenger = result.args.passenger
        flight = result.args.flight
        res = result.args.result
        returnings = BigNumber(result.args.returnings)
      }
    })
    // try to claim
    await contract.claim(flight2, {from: user1})
    // wait for the event to fire
    await wait(1000)
    // assert status
    assert.equal(event, true, 'event fired')
    assert.equal(passenger, user1, 'event fired passenger is correct')
    assert.equal(flight, flight2, 'event fired airline is correct')
    assert.equal(res, false, 'event fired result is correct')
    assert.equal(returnings, 0, 'event fired returnings is correct')
  })

  it('(claim) passenger can\'t claim if flight status is late - technical', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // register another flight
    await contract.registerFlight(flight3, {from: airline1})
    // book another flight
    await contract.bookFlight(flight3, {from: user1, value: web3.utils.toWei(insurancePayment, 'ether')})
    // set status
    await data.processFlightStatus(flight3, STATUS_CODE_LATE_TECHNICAL, new Date().getTime(), {from: owner})
    // watch the emitted event
    let event = false
    let passenger
    let flight
    let res
    let returnings
    await contract.BookingClaimed({}, (error, result) => {
      if (!error) {
        event = true
        passenger = result.args.passenger
        flight = result.args.flight
        res = result.args.result
        returnings = BigNumber(result.args.returnings)
      }
    })
    // try to claim
    await contract.claim(flight3, {from: user1})
    // wait for the event to fire
    await wait(1000)
    // assert status
    assert.equal(event, true, 'event fired')
    assert.equal(passenger, user1, 'event fired passenger is correct')
    assert.equal(flight, flight3, 'event fired airline is correct')
    assert.equal(res, false, 'event fired result is correct')
    assert.equal(returnings, 0, 'event fired returnings is correct')
  })

  it('(claim) passenger can\'t claim if flight status is late - other', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // register another flight
    await contract.registerFlight(flight4, {from: airline1})
    // book another flight
    await contract.bookFlight(flight4, {from: user1, value: web3.utils.toWei(insurancePayment, 'ether')})
    // set status
    await data.processFlightStatus(flight4, STATUS_CODE_LATE_OTHER, new Date().getTime(), {from: owner})
    // watch the emitted event
    let event = false
    let passenger
    let flight
    let res
    let returnings
    await contract.BookingClaimed({}, (error, result) => {
      if (!error) {
        event = true
        passenger = result.args.passenger
        flight = result.args.flight
        res = result.args.result
        returnings = BigNumber(result.args.returnings)
      }
    })
    // try to claim
    await contract.claim(flight4, {from: user1})
    // wait for the event to fire
    await wait(1000)
    // assert status
    assert.equal(event, true, 'event fired')
    assert.equal(passenger, user1, 'event fired passenger is correct')
    assert.equal(flight, flight4, 'event fired airline is correct')
    assert.equal(res, false, 'event fired result is correct')
    assert.equal(returnings, 0, 'event fired returnings is correct')
  })

  it('(claim) passenger can claim if flight status is late - airline', async () => {
    const contract = await FlightSuretyApp.deployed()
    const data = await FlightSuretyData.deployed()
    // set status
    await data.processFlightStatus(flight1, STATUS_CODE_LATE_AIRLINE, new Date().getTime(), {from: owner})
    // watch the emitted event
    let event = false
    let passenger
    let flight
    let res
    let returnings
    await contract.BookingClaimed({}, (error, result) => {
      if (!error) {
        event = true
        passenger = result.args.passenger
        flight = result.args.flight
        res = result.args.result
        returnings = BigNumber(result.args.returnings)
      }
    })
    // try to claim
    await contract.claim(flight1, {from: user1})
    // wait for the event to fire
    await wait(1000)
    // assert status
    assert.equal(event, true, 'event fired')
    assert.equal(passenger, user1, 'event fired passenger is correct')
    assert.equal(flight, flight1, 'event fired airline is correct')
    assert.equal(res, true, 'event fired result is correct')
    assert.equal(returnings, web3.utils.toWei(insuranceReturnings, 'ether'), 'event fired returnings is correct')
  })

  it('(claim) passenger can\'t claim the same flight twice', async () => {
    const contract = await FlightSuretyApp.deployed()
    // make it fail, try to claim twice
    let failure = false
    try {
      await contract.claim(flight1, {from: user1})
    } catch (error) {
      failure = true
    }
    // assert failure
    assert.equal(failure, true, 'claiming should fail')
  })

  it('(withdraw) passenger can extract all funds related to his/her account', async () => {
    const contract = await FlightSuretyApp.deployed()
    // watch the emitted event
    let event = false
    let passenger
    let value
    await contract.FundsWithdrawed({}, (error, result) => {
      if (!error) {
        event = true
        passenger = result.args.passenger
        value = BigNumber(result.args.value)
      }
    })
    // withdraw the funds
    await contract.withdraw({from: user1})
    // wait for the event to fire
    await wait(1000)
    // assert events and data
    assert.equal(event, true, 'event fired')
    assert.equal(passenger, user1, 'event fired passenger is correct')
    assert.equal(value, web3.utils.toWei(insuranceReturnings, 'ether'), 'event fired value is correct')
  })

  it('(withdraw) passenger can\'t extract funds when are 0', async () => {
    const contract = await FlightSuretyApp.deployed()
    // try to withdraw the funds
    let failure = false
    try {
      await contract.withdraw({from: user1})
    } catch (error) {
      failure = true
    }
    // assert failure
    assert.equal(failure, true, 'withdraw failed because funds are 0')
  })

})
